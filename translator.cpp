#include <utility>

#include <QDebug>
#include <QFile>
#include <QStringListModel>
#include <QTextStream>
#include <QtXml>
#include "translator.hpp"

Translator::Translator(QObject *parent) : QObject(parent) {
  // qDebug() << "Init Re-Translator XML file!";
  connect(this, &Translator::run, [this] {
    if (!_nameIn.isEmpty()) Read();
  });
}

void Translator::setTagName(const QString &tag) { _tagName = tag; }

void Translator::setText(const QString &txt) { _text = txt; }

void Translator::Read() {
  qDebug() << "\n[*] Read " << _nameIn << " File";
  QDomDocument xdoc(_nameIn);
  QFile _xml(_nameIn);
  if (!_xml.open(QIODevice::ReadOnly)) return;
  if (!xdoc.setContent(&_xml)) {
    _xml.close();
    return;
  }
  _xml.close();

  QDomElement docElem = xdoc.documentElement();
  auto model = new QStringListModel(this);
  QDomNode n = docElem.firstChild();
  int row = 0;
  // QStringList ss;
  while (!n.isNull()) {
    QDomElement e = n.toElement();  // try to convert the node to an element.
    if (!e.isNull()) {
      _tagName = e.tagName();
      ss << e.text();  // the node really is an element.
    }
    n = n.nextSibling();
    ++row;
  }
  model->setStringList(ss);
  rowCount = model->rowCount();
  QDomNodeList start = xdoc.elementsByTagName(_tagName);
  for (int i = 0; i < row; ++i) {
    QDomNode node = start.item(i);
    auto map = node.attributes();
    for (int j = 0; j < map.length(); j++) {
      auto mapItem = map.item(j);
      auto attribute = mapItem.toAttr();
      _attrN << attribute.name();
      _attrV << attribute.value();
    }
    // qDebug() << "Reading strings: " << _attrV.value(i);
  }
}

void Translator::Open(const QString &name) {
  _nameIn = name;
  emit run();
}

void Translator::outputName(const QString &out) {
  _nameOut = out;
  // Write();
}

void Translator::Write() {
  qDebug() << "\n[*] Write " << _nameOut << " File";
  QDomDocument doc(QFileInfo(_nameOut).baseName());
  QFile sXml(_nameOut);
  if (!sXml.open(QIODevice::WriteOnly | QIODevice::Text)) return;

  QDomElement root = doc.createElement("contentList");
  QTextStream stream(&sXml);
  // int row = 0;
  int col = 1;
  for (int i = 0; i < rowCount; ++i) {
    QDomElement tagFile = doc.createElement(_tagName);
    for (int j = 0; j < col; j++) {
      QDomText de = doc.createTextNode(ss.value(i));
      //.toStdString().c_str());
      tagFile.appendChild(de);
      tagFile.setAttribute(_attrN.value(i), _attrV.value(i));
      // qDebug() << "Writing strings: " << _attrV.value(i);
    }
    root.appendChild(tagFile);
  }
  doc.appendChild(root);
  stream << doc.toString();
  sXml.close();
}

void Translator::Compare(const Translator &b) {
  qDebug() << "\n[*] Comparing hash values";
  int i = 0;
  while (i != b.rowCount) {
    // (int i = 0; i < b.rowCount; i++) {  // Contar english
    // pos i=1;j=0
    for (int j = 0; j < b.rowCount; j++) {         // Contar portuguese
      if (_attrV.value(i) == b._attrV.value(j)) {  // comparar
        ss.replace(i, b.ss.value(j));
        // qDebug().nospace() << "Hash Found:[" << j << "] " <<
        // qPrintable(b._attrV.value(j));
      }
    }
    // qDebug().nospace() << "New Hash found:[" << i << "] " <<
    // qPrintable(_attrV.value(i));
    ++i;
  }
  qDebug() << "\nStrings Total: " << rowCount;
  qDebug() << "Strings replaced Total: " << b.rowCount;
}

QString Translator::getTagName() const { return _tagName; }

QString Translator::getText() const { return _text; }
