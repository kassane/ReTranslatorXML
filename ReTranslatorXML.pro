QT -= gui
QT += xml

CONFIG += c++1z console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

MOC_DIR = $${OUT_PWD}/moc
RCC_DIR = $${OUT_PWD}/rcc
UI_DIR = $${OUT_PWD}/ui
unix:OBJECTS_DIR = $${OUT_PWD}/build/o/unix
win32:OBJECTS_DIR = $${OUT_PWD}/build/o/win32
macx:OBJECTS_DIR = $${OUT_PWD}/build/o/mac

isEmpty(TARGET_EXT) {
    win32 {
        TARGET_CUSTOM_EXT = .exe
    }
    macx {
        TARGET_CUSTOM_EXT = .app
    }
} else {
    TARGET_CUSTOM_EXT = $${TARGET_EXT}
}

RC_ICONS += icon.ico
SOURCES += \
        main.cpp \
    translator.cpp

HEADERS += \
    translator.hpp
