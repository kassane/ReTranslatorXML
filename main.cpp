#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDebug>
#include <QTextCodec>
#include <chrono>
#include "translator.hpp"

int main(int argc, char *argv[]) {
  QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
  QCoreApplication a(argc, argv);
  a.setApplicationName("ReTranslator");
  a.setApplicationVersion("1.0");
  // qDebug() << a.applicationName() << " - " << a.applicationVersion();

  QCommandLineParser parser;
  parser.setApplicationDescription(QString("%1 - %2\t\tBy:Kassany\n\n[Help]")
                                       .arg(a.applicationName())
                                       .arg(a.applicationVersion()));
  parser.addHelpOption();
  parser.addVersionOption();

  QCommandLineOption input(QStringList() << "i"
                                         << "input",
                           "Input file", "filename");
  parser.addOption(input);

  QCommandLineOption input2(QStringList() << "t"
                                          << "Translate",
                            "Translated file", "filename");
  parser.addOption(input2);

  // QCommandLineOption num("n", "number argument", "number", "0");  // default
  // value is: 0
  // parser.addOption(num);

  QCommandLineOption output(QStringList() << "o"
                                          << "output",
                            "Output file", "filename", "output.xml");
  parser.addOption(output);

  parser.process(a);

  if (argc < 2) qDebug() << qPrintable(parser.helpText());

  // qDebug() << parser.value(num);  // = 0
  // const QStringList args = parser.positionalArguments();
  // source is args.at(0), destination is args.at(1)

  else {
    Translator t, t2;  //(parser.value(input), parser.value(output));

    if (parser.isSet(input) && parser.isSet(input2)) {
      qDebug() << "Starting files..." << endl;
      auto start = std::chrono::high_resolution_clock::now();
      t.Open(parser.value(input));
      t2.Open(parser.value(input2));
      t.Compare(t2);
      t.outputName(parser.value(output));
      auto end = std::chrono::high_resolution_clock::now();
      const auto times =
          std::chrono::duration<double, std::kilo>(end - start).count();
      qDebug() << "Duration:" << times << "secs";
    }
    // if (parser.isSet(output)) {
    //    qDebug() << parser.value(output);
    //  t.outputName(parser.value(output));
    //}
    t.Write();
  }

  return 0;  // a.exec();
}
