#include <utility>

#ifndef TRANSLATOR_HPP
#define TRANSLATOR_HPP

#include <QObject>
#pragma once

class QStringListModel;

class Translator : public QObject {
  Q_OBJECT
 public:
  explicit Translator(QObject* parent = nullptr);
  Translator(const QString& input, const QString& output)
      : _nameIn(std::move(input)), _nameOut(output) {
    Read();
    Write();
  }
  void setTagName(const QString& tag);
  void setText(const QString& txt);
  void Read();
  void Open(const QString& name);
  void outputName(const QString& out);
  void Compare(const Translator& b);
  void Write();
  QString getTagName() const;
  QString getText() const;
  ~Translator() = default;

 signals:
  void run();
  void write();

 private:
  QStringList _attrV, _attrN, ss;
  int rowCount = 0;
  QString _tagName, _text, _nameIn, _nameOut;
};

#endif  // TRANSLATOR_HPP
